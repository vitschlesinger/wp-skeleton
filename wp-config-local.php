<?php 
// Local server settings
 
// Local Database
/** The name of the database for WordPress */
define('DB_NAME', 'dbname');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

// Overwrites the database to save keep edeting the DB
define('WP_HOME','http://clientsite.dev');
define('WP_SITEURL','http://clientsite.dev');
 
// Turn on debug for local environment
define('WP_DEBUG', true);