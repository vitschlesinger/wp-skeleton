
What is WP Skeleton?
--------------------
If you are using WP and Git together, you definitely want to have two enviroment. One for development on your localhost and the other one on production server. But, there is problem with deployment with wp-config.php file. When you pushing changes from your dev enviroment to production server, you always have to overwrite wp-config.php lines to connect to actual database and keeping it on mind. Well, we solved this. 

How to use it?
--------------

 1. Install WordPress locally
 2. Create new database also locally
 3. Create wp-config-local.php an add to it code from this repo
 3. Add wp-config-local.php to .gitignore file
 3. Follow instruction bellow

Once you are done, add PHP code from this (current repo) wp-config.php to your wp-config.php file. Replace these lines from your wp-config.php file: 

    /** The name of the database for WordPress */
    define('DB_NAME', 'example');
    
    /** MySQL database username */

    /** MySQL database password */
    define('DB_PASSWORD', 'example');
    
    /** MySQL hostname */
    define('DB_HOST', 'localhost');

> After replacing don't forget to fill correct values to each field you filled.

WP Multisite
-------

If you are running WP Multisite feature, at these lines to each of wp-config files otherwise the Network Admin part will have incorrect link. 

Add at the **end of else part** to **wp-config.php:** 

    // WP Multisite production URL
    define('DOMAIN_CURRENT_SITE', 'clientsite.com');

Add at the **end of else part** to **wp-config-local.php:** 

    // WP Multisite localhost URL
    define('DOMAIN_CURRENT_SITE', 'clientsite.dev');