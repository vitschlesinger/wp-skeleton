<?php

// Use these settings on the local server
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  
// Otherwise use the below settings (on live server)
} else {

	/** The name of the database for WordPress */
	define('DB_NAME', 'dbname');

	/** MySQL database username */
	define('DB_USER', 'dbuser');

	/** MySQL database password */
	define('DB_PASSWORD', 'dbpass');

	/** MySQL hostname */
	define('DB_HOST', 'dbhost');

	// Overwrites the database to save keep edeting the DB
	define('WP_HOME','http://clientsite.com');
	define('WP_SITEURL','http://clientsite.com');
}